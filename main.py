from dronekit import connect, VehicleMode, mavutil
import time

def printDroneStatus():
    print("\nDrone Status")
    print(" Global Location: %s" % vehicle.location.global_frame)
    print(" Global Location (relative altitude): %s" % vehicle.location.global_relative_frame)
    print(" Local Location: %s" % vehicle.location.local_frame)
    print(" Attitude: %s" % vehicle.attitude)
    print(" Velocity: %s" % vehicle.velocity)
    print(" GPS: %s" % vehicle.gps_0)
    print(" EKF OK?: %s" % vehicle.ekf_ok)
    print(" Battery: %s" % vehicle.battery)
    print(" Is Armable?: %s" % vehicle.is_armable)
    print(" System status: %s" % vehicle.system_status.state)
    print(" Groundspeed: %s" % vehicle.groundspeed)    # settable
    print(" Airspeed: %s" % vehicle.airspeed)    # settable
    print(" Mode: %s" % vehicle.mode.name)    # settable
    print(" Armed: %s" % vehicle.armed)    # settable
    
def connectDrone(conString, baud):
    print('Connecting to Drone!')
    vehicle = connect(conString, baud=baud)
    print('Connection established!, waiting 10 sec!')
    time.sleep(10)
    return vehicle

def armVehicle():
    # Copter should arm in GUIDED mode
        
    print("Arming motors")
    vehicle.armed = True

    # Confirm vehicle armed before attempting to take off
    while not vehicle.armed:
        print(" Waiting for arming...")
        time.sleep(1)
        
    vehicle.mode = VehicleMode("GUIDED")
    print('Vehicle is armed!')

    time.sleep(5)

def takeOff(alt):
    print("Taking off!")
    vehicle.simple_takeoff(alt)
    while True:
        print(" Altitude: ", vehicle.location.global_relative_frame.alt)
        if vehicle.location.global_relative_frame.alt >= alt * 0.95:
            print("Reached %s Feet!", alt)
            break
        time.sleep(1)


def landDrone():
    vehicle.mode = VehicleMode("LAND")
    while True:
        print(" Altitude: ", vehicle.location.global_relative_frame.alt)
        if vehicle.location.global_relative_frame.alt >= 1 * 0.95:
            print('Landed!')
            break
        time.sleep(1)

def sendNedVelocity(velocity_x, velocity_y, velocity_z, duration):
    msg = vehicle.message_factory.set_position_target_local_ned_encode(
        0,       # time_boot_ms (not used)
        0, 0,    # target system, target component
        mavutil.mavlink.MAV_FRAME_LOCAL_NED, # frame
        0b0000111111000111, # type_mask (only speeds enabled)
        0, 0, 0, # x, y, z positions (not used)
        velocity_x, velocity_y, velocity_z, # x, y, z velocity in m/s
        0, 0, 0, # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
        0, 0)    # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)


    # send command to vehicle on 1 Hz cycle
    for x in range(0,duration):
        vehicle.send_mavlink(msg)
        time.sleep(1)

vehicle = connectDrone('/dev/tty.usbserial-01E0430C', 57600)
printDroneStatus()
armVehicle()
takeOff(3)
time.sleep(5)
# Set up velocity mappings
# velocity_x > 0 => fly North
# velocity_x < 0 => fly South
# velocity_y > 0 => fly East
# velocity_y < 0 => fly West
# velocity_z < 0 => ascend
# velocity_z > 0 => descend
SOUTH=-2
UP=-0.5   #NOTE: up is negative!

#Fly south and up.
sendNedVelocity(SOUTH,0,UP,3)
landDrone()
